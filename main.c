#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

struct board {
    char unit;
    int graveNumber;
    int teamIndicator;
} gameBoard[8][8];

int turn = 1;       // Whites Turn Is Always First !!
// Its Value Will Be Changed To turn = - turn; ONLY After A Successful Movement .
int startMenu();

void areYouSure();

void setFreshBoard();

int gameLoop();

void loadBoard();

int main() {
    while (1) {
        int option = startMenu();

        while (option == 3) {
            areYouSure();
            option = startMenu();
        }

        if (option == 1)
            loadBoard();
        else if (option == 2)
            setFreshBoard();

        while (gameLoop()) {}
    }

    return 0;
}

void beforeSelectedOption(int, int);

void afterSelectedOption(int, int);

FILE *fPtr = NULL;

int startMenu() { // Disabled Some Options , Because There Is No Time !!
    int keyPressed = 0, minOption = 2, maxOption = 3;

    fPtr = fopen("resume.txt", "r");
    if (fPtr)
        minOption = 1;

    int position = minOption;

    while (keyPressed != 13 /*Enter Key*/ ) {

        system("cls");
        printf("\n\n\n");

        puts("               * ***      *                                               ");
        puts("             *  ****  * **                                                ");
        puts("            *  *  ****  **                                                ");
        puts("           *  **   **   **                                                ");
        puts("          *  ***        **                     ****       ****            ");
        puts("         **   **        **  ***      ***      * **** *   * **** *         ");
        puts("         **   **        ** * ***    * ***    **  ****   **  ****          ");
        puts("         **   **        ***   ***  *   ***  ****       ****               ");
        puts("         **   **        **     ** **    ***   ***        ***              ");
        puts("         **   **        **     ** ********      ***        ***            ");
        puts("          **  **        **     ** *******         ***        ***          ");
        puts("           ** *      *  **     ** **         ****  **   ****  **          ");
        puts("            ***     *   **     ** ****    * * **** *   * **** *           ");
        puts("             *******    **     **  *******     ****       ****            ");
        puts("               ***       **    **   *****                                 ");
        puts("                               * ");
        puts("                              * ");
        puts("                             * ");
        puts("                            * ");
        printf("\n\n");   // 83 Columns Width .

        if (fPtr) {
            for (int i = 0; i < (38 - 6); i++) { printf(" "); }
            beforeSelectedOption(1, position);
            puts("Resume Game");
            afterSelectedOption(1, position);
        }
        for (int i = 0; i < (38 - 5); i++) { printf(" "); }
        beforeSelectedOption(2, position);
        puts("Start New");
        afterSelectedOption(2, position);
        for (int i = 0; i < (38 - 5); i++) { printf(" "); }
        beforeSelectedOption(3, position);
        puts("Exit Game");
        afterSelectedOption(3, position);

        keyPressed = (int) getch();

        if (keyPressed == 80)
            position++;
        else if (keyPressed == 72)
            position--;
        else if (keyPressed == 27)
            areYouSure();

        if (position > maxOption)
            position = minOption;
        else if (position < minOption)
            position = maxOption;

    }   // End While

    return position;
}

void SetColorAndBackground(int, int);

void beforeSelectedOption(int realPosition, int arrowPosition) { // Highlights It With A Normal White Color.
    if (realPosition == arrowPosition)
        SetColorAndBackground(0, 7);
}   // https://youtu.be/StEBDXY2WBk

void afterSelectedOption(int realPosition, int arrowPosition) { // Return The Text After It To The Default.
    if (realPosition == arrowPosition)
        SetColorAndBackground(7, 0);
}   // https://youtu.be/StEBDXY2WBk

void SetColorAndBackground(int ForgC, int BackC) {
    WORD wColor = ((BackC & 0x0F) << 4) + (ForgC &
                                           0x0F);                  // Takes The First 4 Digits Of The Number As They Are Only From 0 To 15
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
                            wColor);      // Just Like system("COLOR [Back][Fore]"); But More Efficient !
}   // https://www.codewithc.com/highlight-change-text-background-color-in-codeblocks-console-window/

void areYouSure() { // If Yes exit(0) ,If No Just Continue ( Back To Previous Window ).
    int keyPressed = 0, position = 1;

    while (keyPressed != 13 /*Enter Key*/ ) {

        system("cls");
        printf("\n\n\n");

        printf("\t%c", 218);
        for (int i = 0; i < 40; i++) { printf("%c", 196); }
        printf("%c\n", 191);
        printf("\t%c", 179);
        printf(" Are you sure you want to exit ?        ");
        printf("%c\n", 179);
        printf("\t%c", 179);
        for (int i = 0; i < 25; i++) { printf(" "); }
        printf("%c%c%c%c%c%c%c %c%c%c%c%c%c %c\n", 218, 196, 196, 196, 196, 196, 191, 218, 196, 196, 196, 196, 191,
               179);
        printf("\t%c", 179);
        for (int i = 0; i < 25; i++) { printf(" "); }
        printf("%c ", 179);

        beforeSelectedOption(1, position);
        printf("Yes");
        afterSelectedOption(1, position);
        printf(" %c %c ", 179, 179);
        beforeSelectedOption(2, position);
        printf("No");
        afterSelectedOption(2, position);
        printf(" %c %c\n", 179, 179);

        printf("\t%c", 179);
        for (int i = 0; i < 25; i++) { printf(" "); }
        printf("%c%c%c%c%c%c%c %c%c%c%c%c%c %c\n", 192, 196, 196, 196, 196, 196, 217, 192, 196, 196, 196, 196, 217,
               179);
        printf("\t%c", 192);
        for (int i = 0; i < 40; i++) { printf("%c", 196); }
        printf("%c\n", 217);

        keyPressed = (int) getch();

        if ((keyPressed == 75) || (keyPressed == 77))
            if (position == 1)
                position = 2;
            else
                position = 1;

        if (keyPressed == 27)
            break;
    }
    if ((position == 1) && (keyPressed == 13))
        exit(0);
}

void setFreshBoard() { // Just For Start New Game Option .

    turn = 1;
    char board[8][8] = {{'r',  'n',  'b',  'q',  'k',  'b',  'n',  'r'},
                        {'p',  'p',  'p',  'p',  'p',  'p',  'p',  'p'},
                        {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},
                        {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},
                        {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},
                        {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},
                        {'P',  'P',  'P',  'P',  'P',  'P',  'P',  'P'},
                        {'R',  'N',  'B',  'Q',  'K',  'B',  'N',  'R'}};

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
            gameBoard[i][j].unit = board[i][j];

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++) {
            if (i < 2) {
                gameBoard[i][j].graveNumber = (j + 1) + (8 * i);
                gameBoard[i][j].teamIndicator = 1;
            } else if (i >= 6) {
                gameBoard[i][j].graveNumber = (j + 1) + (8 * (7 - i));
                gameBoard[i][j].teamIndicator = -1;
            } else {
                gameBoard[i][j].graveNumber = 0;
                gameBoard[i][j].teamIndicator = 0;
            }
        }
}

void calculateTeamIndicators();

void loadBoard() {
    char convertedGrave[8][8], currentLine[18];

    fPtr = fopen("resume.txt", "r");
    int cL = 0;

    while (!feof(fPtr)) {
        fgets(currentLine, 18, fPtr);
        if (cL < 8) {                                      // First Load The Board.
            for (int i = 0; i < 8; i++)
                gameBoard[cL][i].unit = currentLine[2 * i];
        } else if (cL < 16) {
            for (int i = 0; i < 8; i++)
                convertedGrave[cL - 8][i] = currentLine[2 * i];
        } else if (cL == 16)
            turn = currentLine[0] - 50;
        cL++;
    }

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
            gameBoard[i][j].graveNumber = (convertedGrave[i][j] > 60) ? convertedGrave[i][j] - 54 :
                                          convertedGrave[i][j] - 47; // 1 <-- 0 && 11 <-- A . && 0 <-- /

    calculateTeamIndicators();
}

void calculateTeamIndicators() {
    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++) {
            if (gameBoard[i][j].unit == '\0')
                gameBoard[i][j].teamIndicator = 0;
            else if (gameBoard[i][j].unit > 97)
                gameBoard[i][j].teamIndicator = 1;
            else
                gameBoard[i][j].teamIndicator = -1;
        }
}

void printBoard();

void inPut(char *, int *);

int doMove(int *);

int undoS = 0;
redoS = 0, turnS = 0;

void undo();

void redo();

int checkCheck(int);

int checkIfAnyCanMove(int);

struct history {
    char units[8][8];
    int graveNumbers[8][8];
} gameHistory[5899];        //https://lichess.org/forum/general-chess-discussion/official-draw-rule-in-chess-also--theoretical-maximum-number-of-moves-in-one-game

int gameLoop() { // Return 0 If GAME OVER ! ( Win Or Stalemate ).
    int moveOctal[5], gameOn = 1, test = 0;
    char order[6];

    while (gameOn) {

        printBoard();

        switch (test) {
            case -1  :
                printf("   Way is blocked : please try again.   ");
                break;
            case 1  :
                printf("    Empty square : please try again.    ");
                break;
            case 2  :
                printf("    Invalid move : please try again.    ");
                break;
            case 3  :
                printf("   It isn't yours : please try again.   ");
                break;
            case 4  :
                printf("      You have promoted your pawn!      ");
                printf("\a");
                break;
            case 5  :
                printf("   The same point : please try again.   ");
                break;
            case 10   :
                printf(" Your king is in danger : forced undo ! ");
                break;
            case 100  :
                printf("    You are checked : do something !    ");
                printf("\a");
                break;
            case 1000 :
                if (turn == 1) /*Then -1 (BLACK WON)*/ {
                    printf("\a");
                    printf("   Oh, checkmate : hard luck friend.    \n\a");
                    Sleep(2500);
                    printf(" Congratulations : player two has won ! \n\a");
                    getch();
                    return 0;
                } else {
                    printf("\a");
                    printf("   Oh, checkmate : hard luck friend.    \n\a");
                    Sleep(2500);
                    printf(" Congratulations : player one has won ! \n\a");
                    getch();
                    return 0;
                }
            case 2000 :    /*Draw !!*/
                printf("\a");
                printf("    Ops, stalemate : hard luck guys.    ");
                Sleep(5000);
                getch();
                return 0;

            default :
                printf("\t\t\t\t\t");
                printf("\a");
                break;
        }

        for (int i = 0; i < 5; i++) { moveOctal[i] = 0; }
        inPut(order, moveOctal);
        test = doMove(moveOctal);                               // When 0 , 4 Move Is Done !
        // Returns 0 : If You Can Move , Returns 2000 : If Stalemate .
        int noOneCanMove;
        noOneCanMove = checkIfAnyCanMove(turn);

        int forceUndo;
        forceUndo = checkCheck(-turn);   // Returns 0  : Your Are Safe To Go .. Let The Other Play .
        // Returns 10 : You Caused Your Own Check .... Force Undo .
        if (forceUndo) {
            test = 10;
            undo();
            redoS = 0;
            continue;
        }

        int check;
        check = checkCheck(turn);    // Returns 0   : Your Enemy Is Safe To Go .. Let Him Play .
        // Returns 100 : You Checked Your Enemy .

        if (noOneCanMove && check)
            test = 1000;
        else if (noOneCanMove)
            test = 2000;
        else if (check)
            test = 100;
    }

    return 0;
}

void printBoard() {
    system("cls"); // -> Windows, for UNIX -> system("clear");
    char boardgaps[8][8], board[8][8];

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
            board[i][j] = gameBoard[i][j].unit;

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++) {
            boardgaps[7 - i][j] = (i + j) % 2 ? ':'
                                              : ' '; // If Row Actual + Column Actual = Even Number , Then It Must Be A White Square .
            if (board[7 - i][j] != '\0')
                boardgaps[7 -
                          i][j] = ' '; // If A Square Has Text Inside It Add Space Before & After It " For Clarity " .
            else
                board[7 - i][j] =
                        (i + j) % 2 ? ':' : ' '; // If A Square Has Nothing In It Make It Follow The Gaps Pattern .
        }

    char blackGrave[16], whiteGrave[16];

    for (int j = 0; j < 16; j++) {      // Filled With Spaces
        blackGrave[j] = 32;
        whiteGrave[j] = 32;
    }

    for (int k = 1; k <= 16; k++) {
        int found = 0;
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++) {
                if ((gameBoard[i][j].graveNumber == k) &&
                    (gameBoard[i][j].teamIndicator == -1)) {            // Check For Dead Black Units .
                    found = 1;
                    break;
                }
            }
        if (found == 0) {
            blackGrave[k - 1] = 88;
        }
    }

    for (int k = 1; k <= 16; k++) {
        int found = 0;
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++) {
                if ((gameBoard[i][j].graveNumber == k) &&
                    (gameBoard[i][j].teamIndicator == 1)) {            // Check For Dead White Units .
                    found = 1;
                    break;
                }
            }
        if (found == 0) {
            whiteGrave[k - 1] = 120;
        }
    }

    printf("\n");
    //These Line Will Be SOo Wide , Just To Look Better .
    // Rows Names Add To The Main Board Design (Because They Are In The Same Line !).
    printf("           ------- ------- ------- ------- ------- ------- ------- ------- \n");
    printf("   %c%c%c%c%c  |       |:::::::|       |:::::::|       |:::::::|       |:::::::|", 214, 196, 196, 196, 183);
    printf("\t\t\t\t\t\t          ( Taken Out )          \n");
    printf("   %c %c %c  |", 186, '8', 186);
    printf("  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|", boardgaps[7][0],
           board[7][0], boardgaps[7][0], boardgaps[7][1], board[7][1], boardgaps[7][1], boardgaps[7][2], board[7][2],
           boardgaps[7][2], boardgaps[7][3], board[7][3], boardgaps[7][3], boardgaps[7][4], board[7][4],
           boardgaps[7][4], boardgaps[7][5], board[7][5], boardgaps[7][5], boardgaps[7][6], board[7][6],
           boardgaps[7][6], boardgaps[7][7], board[7][7], boardgaps[7][7]);
    printf("\t\t\t\t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 201, 205, 205, 205, 209,
           205, 205, 205, 209, 205, 205, 205, 209, 205, 205, 205, 209, 205, 205, 205, 209, 205, 205, 205, 209, 205, 205,
           205, 209, 205, 205, 205, 187);
    printf("   %c%c%c%c%c  |       |:::::::|       |:::::::|       |:::::::|       |:::::::|", 211, 196, 196, 196, 189);
    printf("\t\t\t\t\t\t%c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c\n", 186, blackGrave[0], 179, blackGrave[1],
           179, blackGrave[2], 179, blackGrave[3], 179, blackGrave[4], 179, blackGrave[5], 179, blackGrave[6], 179,
           blackGrave[7], 186);
    printf("           -------o-------o-------o-------o-------o-------o-------o------- ");
    printf("\t\t\t\t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 199, 196, 196, 196, 197,
           196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196,
           196, 197, 196, 196, 196, 182);
    printf("   %c%c%c%c%c  |:::::::|       |:::::::|       |:::::::|       |:::::::|       |", 214, 196, 196, 196, 183);
    printf("\t\t\t\t\t\t%c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c\n", 186, blackGrave[8], 179, blackGrave[9],
           179, blackGrave[10], 179, blackGrave[11], 179, blackGrave[12], 179, blackGrave[13], 179, blackGrave[14], 179,
           blackGrave[15], 186);
    printf("   %c %c %c  |", 186, '7', 186);
    printf("::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |", boardgaps[6][0],
           board[6][0], boardgaps[6][0], boardgaps[6][1], board[6][1], boardgaps[6][1], boardgaps[6][2], board[6][2],
           boardgaps[6][2], boardgaps[6][3], board[6][3], boardgaps[6][3], boardgaps[6][4], board[6][4],
           boardgaps[6][4], boardgaps[6][5], board[6][5], boardgaps[6][5], boardgaps[6][6], board[6][6],
           boardgaps[6][6], boardgaps[6][7], board[6][7], boardgaps[6][7]);
    printf("\t\t\t\t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 199, 196, 196, 196, 197,
           196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196,
           196, 197, 196, 196, 196, 182);
    printf("   %c%c%c%c%c  |:::::::|       |:::::::|       |:::::::|       |:::::::|       |", 211, 196, 196, 196, 189);
    printf("\t\t\t\t\t\t%c   %c   %c   %c   %c   %c   %c   %c   %c\n", 186, 179, 179, 179, 179, 179, 179, 179, 186);
    printf("           -------o-------o-------o-------o-------o-------o-------o------- ");
    printf("\t\t\t\t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 199, 196, 196, 196, 197,
           196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196,
           196, 197, 196, 196, 196, 182);
    printf("   %c%c%c%c%c  |       |:::::::|       |:::::::|       |:::::::|       |:::::::|", 214, 196, 196, 196, 183);
    printf("\t\t\t\t\t\t%c   %c   %c   %c   %c   %c   %c   %c   %c\n", 186, 179, 179, 179, 179, 179, 179, 179, 186);
    printf("   %c %c %c  |", 186, '6', 186);
    printf("  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|", boardgaps[5][0],
           board[5][0], boardgaps[5][0], boardgaps[5][1], board[5][1], boardgaps[5][1], boardgaps[5][2], board[5][2],
           boardgaps[5][2], boardgaps[5][3], board[5][3], boardgaps[5][3], boardgaps[5][4], board[5][4],
           boardgaps[5][4], boardgaps[5][5], board[5][5], boardgaps[5][5], boardgaps[5][6], board[5][6],
           boardgaps[5][6], boardgaps[5][7], board[5][7], boardgaps[5][7]);
    printf("\t\t\t\t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 199, 196, 196, 196, 197,
           196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196,
           196, 197, 196, 196, 196, 182);
    printf("   %c%c%c%c%c  |       |:::::::|       |:::::::|       |:::::::|       |:::::::|", 211, 196, 196, 196, 189);
    printf("\t\t\t\t\t\t%c   %c   %c   %c   %c   %c   %c   %c   %c\n", 186, 179, 179, 179, 179, 179, 179, 179, 186);
    printf("           -------o-------o-------o-------o-------o-------o-------o------- ");
    printf("\t\t\t\t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 199, 196, 196, 196, 197,
           196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196,
           196, 197, 196, 196, 196, 182);
    printf("   %c%c%c%c%c  |:::::::|       |:::::::|       |:::::::|       |:::::::|       |", 214, 196, 196, 196, 183);
    printf("\t\t\t\t\t\t%c   %c   %c   %c   %c   %c   %c   %c   %c\n", 186, 179, 179, 179, 179, 179, 179, 179, 186);
    printf("   %c %c %c  |", 186, '5', 186);
    printf("::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |", boardgaps[4][0],
           board[4][0], boardgaps[4][0], boardgaps[4][1], board[4][1], boardgaps[4][1], boardgaps[4][2], board[4][2],
           boardgaps[4][2], boardgaps[4][3], board[4][3], boardgaps[4][3], boardgaps[4][4], board[4][4],
           boardgaps[4][4], boardgaps[4][5], board[4][5], boardgaps[4][5], boardgaps[4][6], board[4][6],
           boardgaps[4][6], boardgaps[4][7], board[4][7], boardgaps[4][7]);
    printf("\t\t\t\t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 199, 196, 196, 196, 197,
           196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196,
           196, 197, 196, 196, 196, 182);
    printf("   %c%c%c%c%c  |:::::::|       |:::::::|       |:::::::|       |:::::::|       |", 211, 196, 196, 196, 189);
    printf("\t\t\t\t\t\t%c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c\n", 186, whiteGrave[8], 179, whiteGrave[9],
           179, whiteGrave[10], 179, whiteGrave[11], 179, whiteGrave[12], 179, whiteGrave[13], 179, whiteGrave[14], 179,
           whiteGrave[15], 186);
    printf("           -------o-------o-------o-------o-------o-------o-------o------- ");
    printf("\t\t\t\t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 199, 196, 196, 196, 197,
           196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196, 197, 196, 196,
           196, 197, 196, 196, 196, 182);
    printf("   %c%c%c%c%c  |       |:::::::|       |:::::::|       |:::::::|       |:::::::|", 214, 196, 196, 196, 183);
    printf("\t\t\t\t\t\t%c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c\n", 186, whiteGrave[0], 179, whiteGrave[1],
           179, whiteGrave[2], 179, whiteGrave[3], 179, whiteGrave[4], 179, whiteGrave[5], 179, whiteGrave[6], 179,
           whiteGrave[7], 186);
    printf("   %c %c %c  |", 186, '4', 186);
    printf("  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|", boardgaps[3][0],
           board[3][0], boardgaps[3][0], boardgaps[3][1], board[3][1], boardgaps[3][1], boardgaps[3][2], board[3][2],
           boardgaps[3][2], boardgaps[3][3], board[3][3], boardgaps[3][3], boardgaps[3][4], board[3][4],
           boardgaps[3][4], boardgaps[3][5], board[3][5], boardgaps[3][5], boardgaps[3][6], board[3][6],
           boardgaps[3][6], boardgaps[3][7], board[3][7], boardgaps[3][7]);
    printf("\t\t\t\t\t\t%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 200, 205, 205, 205, 207,
           205, 205, 205, 207, 205, 205, 205, 207, 205, 205, 205, 207, 205, 205, 205, 207, 205, 205, 205, 207, 205, 205,
           205, 207, 205, 205, 205, 188);
    printf("   %c%c%c%c%c  |       |:::::::|       |:::::::|       |:::::::|       |:::::::|\n", 211, 196, 196, 196,
           189);
    printf("           -------o-------o-------o-------o-------o-------o-------o------- \n");
    printf("   %c%c%c%c%c  |:::::::|       |:::::::|       |:::::::|       |:::::::|       |", 214, 196, 196, 196, 183);
    printf("\t\t\t\t\t\t          [ Shortcuts ]\n");
    printf("   %c %c %c  |", 186, '3', 186);
    printf("::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |", boardgaps[2][0],
           board[2][0], boardgaps[2][0], boardgaps[2][1], board[2][1], boardgaps[2][1], boardgaps[2][2], board[2][2],
           boardgaps[2][2], boardgaps[2][3], board[2][3], boardgaps[2][3], boardgaps[2][4], board[2][4],
           boardgaps[2][4], boardgaps[2][5], board[2][5], boardgaps[2][5], boardgaps[2][6], board[2][6],
           boardgaps[2][6], boardgaps[2][7], board[2][7], boardgaps[2][7]);
    printf("\t\t\t\t\t\t       %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 218, 196, 196, 196, 196, 196, 196, 194, 196,
           196, 196, 196, 196, 196, 196, 196, 196, 196, 191);
    printf("   %c%c%c%c%c  |:::::::|       |:::::::|       |:::::::|       |:::::::|       |", 211, 196, 196, 196, 189);
    printf("\t\t\t\t\t\t       %c Save %c Ctrl + S %c\n", 179, 179, 179);
    printf("           -------o-------o-------o-------o-------o-------o-------o------- ");
    printf("\t\t\t\t\t\t       %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 195, 196, 196, 196, 196, 196, 196, 197, 196,
           196, 196, 196, 196, 196, 196, 196, 196, 196, 180);
    printf("   %c%c%c%c%c  |       |:::::::|       |:::::::|       |:::::::|       |:::::::|", 214, 196, 196, 196, 183);
    printf("\t\t\t\t\t\t       %c Redo %c Ctrl + Y %c\n", 179, 179, 179);
    printf("   %c %c %c  |", 186, '2', 186);
    printf("  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|", boardgaps[1][0],
           board[1][0], boardgaps[1][0], boardgaps[1][1], board[1][1], boardgaps[1][1], boardgaps[1][2], board[1][2],
           boardgaps[1][2], boardgaps[1][3], board[1][3], boardgaps[1][3], boardgaps[1][4], board[1][4],
           boardgaps[1][4], boardgaps[1][5], board[1][5], boardgaps[1][5], boardgaps[1][6], board[1][6],
           boardgaps[1][6], boardgaps[1][7], board[1][7], boardgaps[1][7]);
    printf("\t\t\t\t\t\t       %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 195, 196, 196, 196, 196, 196, 196, 197, 196,
           196, 196, 196, 196, 196, 196, 196, 196, 196, 180);
    printf("   %c%c%c%c%c  |       |:::::::|       |:::::::|       |:::::::|       |:::::::|", 211, 196, 196, 196, 189);
    printf("\t\t\t\t\t\t       %c Undo %c Ctrl + Z %c\n", 179, 179, 179);
    printf("           -------o-------o-------o-------o-------o-------o-------o------- ");
    printf("\t\t\t\t\t\t       %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 192, 196, 196, 196, 196, 196, 196, 193, 196,
           196, 196, 196, 196, 196, 196, 196, 196, 196, 217);
    printf("   %c%c%c%c%c  |:::::::|       |:::::::|       |:::::::|       |:::::::|       |\n", 214, 196, 196, 196,
           183);
    printf("   %c %c %c  |", 186, '1', 186);
    printf("::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |::%c%c%c::|  %c%c%c  |\n",
           boardgaps[0][0], board[0][0], boardgaps[0][0], boardgaps[0][1], board[0][1], boardgaps[0][1],
           boardgaps[0][2], board[0][2], boardgaps[0][2], boardgaps[0][3], board[0][3], boardgaps[0][3],
           boardgaps[0][4], board[0][4], boardgaps[0][4], boardgaps[0][5], board[0][5], boardgaps[0][5],
           boardgaps[0][6], board[0][6], boardgaps[0][6], boardgaps[0][7], board[0][7], boardgaps[0][7]);
    printf("   %c%c%c%c%c  |:::::::|       |:::::::|       |:::::::|       |:::::::|       |", 211, 196, 196, 196, 189);
    printf("\t\t\t\t\t\t\t     { Tip }\n");
    printf("           ------- ------- ------- ------- ------- ------- ------- ------- ");
    printf("\t\t\t\t\t      %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 218, 196,
           196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196,
           196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 191);
    //Columns Names , Borders Made By Their ASCII Codes .
    printf("            %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   ",
           213, 205, 205, 205, 184, 213, 205, 205, 205, 184, 213, 205, 205, 205, 184, 213, 205, 205, 205, 184, 213, 205,
           205, 205, 184, 213, 205, 205, 205, 184, 213, 205, 205, 205, 184, 213, 205, 205, 205, 184);
    printf("\t\t\t\t\t      %c You CAN'T enter an invalid input. %c\n", 179, 179);
    printf("            %c %c %c   %c %c %c   %c %c %c   %c %c %c   %c %c %c   %c %c %c   %c %c %c   %c %c %c   ", 179,
           'A', 179, 179, 'B', 179, 179, 'C', 179, 179, 'D', 179, 179, 'E', 179, 179, 'F', 179, 179, 'G', 179, 179, 'H',
           179);
    printf("\t\t\t\t\t      %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n", 192, 196,
           196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196,
           196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 196, 217);
    printf("            %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   %c%c%c%c%c   \n",
           212, 205, 205, 205, 190, 212, 205, 205, 205, 190, 212, 205, 205, 205, 190, 212, 205, 205, 205, 190, 212, 205,
           205, 205, 190, 212, 205, 205, 205, 190, 212, 205, 205, 205, 190, 212, 205, 205, 205, 190);

    if (turn == 1)
        printf("\nPlayer One Turn : ( white units )\n");
    else
        printf("\nPlayer Two Turn : ( BLACK UNITS )\n");
}

void rePrint(char *, int);

int isUpgradeable(int, int);

void saveBoard();

void inPut(char order[],
           int moveOctal[]) { // Consider Undo ( CTRL + Z ) & Redo [Never Allow It If He Didn't Undo , Remove It If He Played Successfully !!] ( CTRL + Y )
    int i = 0, maxLetters = 4;
    char ch;

    while (i <= maxLetters) {

        if (i >= 2) {       // When i >= 2 , Check If You Have A Pawn That Is Able To Be Promoted .

            int stColumn, stRow;          /* Explained Down At The End Of The Function . */
            stColumn = ((order[0]) - 65);  /*A = 65 Will Be Converted To (Int) Value = 0 .*/
            stRow = ((order[1]) - 49);     /*1 = 49 Will Be Converted To (Int) Value = 0 .*/

            if (isUpgradeable(stRow, stColumn))
                maxLetters = 5;
            else
                maxLetters = 4;
        }

        while (1) { // Allow Him To Only Enter ( 1 - 8 ) && ( A - H ) && ( B , N , Q , R ) .

            int done = 0;
            ch = getch();

            if (ch == 19) {            // Ctrl + S --> Save .
                saveBoard();
                continue;
            } else if ((ch == 8) && (i)) {       // If He pressed <- Back Space .
                i--;
                rePrint(order, i);
            } else if (ch == 27) {            // Press ESC
                areYouSure();
                printBoard();
                rePrint(order, i);
                continue;
            } else if ((ch == 26) && (undoS)) {  // Ctrl + U --> Undo .
                undo();
                printBoard();
                i = 0;
                rePrint(order, i);
                continue;
            } else if ((ch == 25) && (redoS)) {  // Ctrl + Y --> Redo .
                redo();
                printBoard();
                i = 0;
                rePrint(order, i);
                continue;
            }

            if (i == maxLetters)
                if (ch == 13)
                    break;
                else
                    continue;

            switch (i) {
                case 0  :
                case 2  :
                    if (ch > 96)
                        ch -= 32;
                    if ((ch > 64) && (ch <= 72)) {
                        printf("%c", ch);
                        done = 1;
                    }
                    break;

                case 1  :
                case 3  :
                    if ((ch > 48) && (ch < 57)) {
                        printf("%c", ch);
                        done = 1;
                    }
                    break;

                case 4  :
                    if (ch > 96)
                        ch -= 32;
                    if ((ch == 66) || (ch == 78) || (ch == 81) || (ch == 82)) {
                        printf("%c", ch);
                        done = 1;
                    }
                    break;

                default :
                    printf("Unknown Error ...\n\ti = %d\n\tch = %c = %d\n", i, ch, ch);
                    exit(0);
            }//End Switch
            if (done)
                break;
        }//End Inner While
        order[i] = ch;
        i++;
    }//End Outer While

    order[i] = '\0';   // Seems Useless ( But I"ll Keep It ) .

    int stColumn = 0, stRow = 0, ndColumn = 0, ndRow = 0, pr = 0;

    /*First Column*/    stColumn = ((order[0]) - 65);  /*A = 65 Will Be Converted To (Int) Value = 0 .*/
    /*First Row*/       stRow = ((order[1]) - 49);     /*1 = 49 Will Be Converted To (Int) Value = 0 .*/
    ndColumn = ((order[2]) - 65);
    ndRow = ((order[3]) - 49);

    switch (order[4]) {
        case 'B' :
            pr = 4;
            break;    /*Bishop = B*/
        case 'N' :
            pr = 3;
            break;    /*Knight = N*/
        case 'Q' :
            pr = 5;
            break;    /*Queen  = Q*/
        case 'R' :
            pr = 2;
            break;    /*Rook   = R*/
        default  :
            pr = 0;
            break;
    }

    moveOctal[0] = stColumn;
    moveOctal[1] = stRow;
    moveOctal[2] = ndColumn;
    moveOctal[3] = ndRow;
    moveOctal[4] = pr;
}

void rePrint(char order[], int i) {
    printf("\33[2K\r"); // Clear Line , Rewind To Its Beginning ( https://stackoverflow.com/questions/1508490/erase-the-current-printed-console-line ) .
    order[i] = '\0';
    printf("\t\t\t\t\t%s", order);
}

void undo() {
    undoS--;
    turnS--;
    redoS++;

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++) {
            gameBoard[i][j].unit = gameHistory[turnS].units[i][j];
            gameBoard[i][j].graveNumber = gameHistory[turnS].graveNumbers[i][j];
        }

    calculateTeamIndicators();
    turn = -turn;
}

void redo() {
    redoS--;
    turnS++;
    undoS++;

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++) {
            gameBoard[i][j].unit = gameHistory[turnS].units[i][j];
            gameBoard[i][j].graveNumber = gameHistory[turnS].graveNumbers[i][j];
        }

    calculateTeamIndicators();
    turn = -turn;
}

void saveBoard() {
    char convertedGrave[8][8];  // DEC to HEX

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
            convertedGrave[i][j] = (gameBoard[i][j].graveNumber > 10) ? gameBoard[i][j].graveNumber + 54 :
                                   gameBoard[i][j].graveNumber + 47; // 1 --> 0 && 11 --> A . && 0 --> /

    fPtr = fopen("resume.txt", "w");

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            fprintf(fPtr, "%c ", gameBoard[i][j].unit);
        }
        fprintf(fPtr, "\n");
    }
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            fprintf(fPtr, "%c ", convertedGrave[i][j]);
        }
        fprintf(fPtr, "\n");
    }
    fprintf(fPtr, "%d", turn + 2);

    fclose(fPtr);
}

int checkUnit(int, int);

int isUpgradeable(int i, int j) {
    int r = i, t = gameBoard[i][j].teamIndicator;

    if (checkUnit(i, j) == 1)
        if ((2 * r) - (5 * t) ==
            7) // Which Satisfies ( 6 *7th row on board* , 1 *white team* ) & ( 1 *2nd row on board* , -1 *BLACK TEAM* ) .
            return 1;

    return 0;
}

int checkUnit(int yNod, int xNod) {
    int unit = 0;

    switch (gameBoard[yNod][xNod].unit) {
        case 'r' :
        case 'R' :   /*Rooks*/    unit = 2;
            break;

        case 'n' :
        case 'N' :   /*Knights*/  unit = 3;
            break;

        case 'b' :
        case 'B' :   /*Bishops*/  unit = 4;
            break;

        case 'q' :
        case 'Q' :   /*A Queen*/  unit = 5;
            break;

        case 'k' :
        case 'K' :   /*The King*/ unit = 6;
            break;

        case 'p' :
        case 'P' :   /*Pawns*/    unit = 1;
            break;

        default :
            if (gameBoard[yNod][xNod].unit != '\0') {
                printf("Unknown Error ...\n\txNod = %d\n\tyNod = %d\n\n\tunit = %c\n", xNod, yNod,
                       gameBoard[yNod][xNod].unit);
                exit(0);
            } else
                break;
    }

    return unit;
}

int movePawn(int, int, int, int, int, int);

int moveRook(int, int, int, int, int);

int moveKnight(int, int, int, int, int);

int moveBishop(int, int, int, int, int);

int moveQueen(int, int, int, int, int);

int moveKing(int, int, int, int, int);

int doMove(int move[]) { //Refuse OR Replace OR Eat !
    int xNod = move[0], yNod = move[1], x = move[2], y = move[3], pr = move[4];
    int invalid = 0;

    if (gameBoard[yNod][xNod].teamIndicator == 0)
        return 1;           // Square Is Empty .
    else if (gameBoard[yNod][xNod].teamIndicator != turn)
        return 3;           // Not Your Unit !!

    if ((y == yNod) && (x == xNod))
        return 5;           // Moving To The Same Point !!

    switch (checkUnit(yNod, xNod)) {
        case 1 :
            invalid = movePawn(yNod, xNod, y, x, pr, 1);
            break;
        case 2 :
            invalid = moveRook(yNod, xNod, y, x, 1);
            break;
        case 3 :
            invalid = moveKnight(yNod, xNod, y, x, 1);
            break;
        case 4 :
            invalid = moveBishop(yNod, xNod, y, x, 1);
            break;
        case 5 :
            invalid = moveQueen(yNod, xNod, y, x, 1);
            break;
        case 6 :
            invalid = moveKing(yNod, xNod, y, x, 1);
            break;

        default:
            return 1; // Square Is Empty .
    }

    if (invalid == 1)
        return 2;          // Invalid Move !
    else if (invalid == -1)
        return -1;         // Something Blocking The Way !
    else if (pr)
        return 4;          // Promotion Success .

    return 0;
}

void iLikeToMoveIt(int, int, int, int);

int movePawn(int yNod, int xNod, int y, int x, int pr, int doIt) {
    int pointT = gameBoard[yNod][xNod].teamIndicator, distinationT = gameBoard[y][x].teamIndicator;
    int Success = 0;

    if (y == (yNod + pointT)) {
        if (x == xNod) {
            if (distinationT == 0)
                Success = 1;                            //It has to be EMPTY !!
        } else if ((x == (xNod + 1)) || (x == (xNod - 1))) {
            if (distinationT == -(pointT))
                Success = 1;                            //It has to be TAKEN !!
        }
    }

    /*Special Case*/
    if (((yNod * 2) == (7 - 5 * pointT)) && (x == xNod) && (y == yNod + (2 * pointT))) {
        if (gameBoard[yNod + pointT][x].teamIndicator == 0)
            if (distinationT == 0)
                Success = 1;
    }

    if (Success) {
        switch (pr) {
            case 2 :
                gameBoard[yNod][xNod].unit = (pointT == 1) ? 'r' : 'R';
                break;
            case 3 :
                gameBoard[yNod][xNod].unit = (pointT == 1) ? 'n' : 'N';
                break;
            case 4 :
                gameBoard[yNod][xNod].unit = (pointT == 1) ? 'b' : 'B';
                break;
            case 5 :
                gameBoard[yNod][xNod].unit = (pointT == 1) ? 'q' : 'Q';
                break;

            default:
                break;
        }

        if (doIt)
            iLikeToMoveIt(yNod, xNod, y, x);
        return 0;
    }

    return 1; // Non-Zero = Invalid
}

int checkTrackHorizontal(int, int, int);

int checkTrackVertical(int, int, int);

int moveRook(int yNod, int xNod, int y, int x, int doIt) {
    int pointT = gameBoard[yNod][xNod].teamIndicator, distinationT = gameBoard[y][x].teamIndicator;
    int Success = 0;

    if (y == yNod)        // Track One
        Success = checkTrackHorizontal(x, xNod, y);

    else if (x == xNod)   // Track Two
        Success = checkTrackVertical(y, yNod, x);

    if (distinationT == pointT)
        Success = 0;

    if (Success == 1) {
        if (doIt)
            iLikeToMoveIt(yNod, xNod, y, x);
        return 0;
    } else if (Success == -1)
        return -1;      // Something Blocking The Way !

    return 1; // One = Invalid
}

int greaterOfTwo(int, int);

int smallerOfTwo(int, int);

int checkTrackHorizontal(int x, int xNod, int yAny) {
    int min = smallerOfTwo(x, xNod), max = greaterOfTwo(x, xNod);

    for (int i = min + 1; i < max; i++)
        if (gameBoard[yAny][i].teamIndicator)
            return -1;
    return 1;
}

int checkTrackVertical(int y, int yNod, int xAny) {
    int min = smallerOfTwo(y, yNod), max = greaterOfTwo(y, yNod);

    for (int i = min + 1; i < max; i++)
        if (gameBoard[i][xAny].teamIndicator)
            return -1;
    return 1;
}

int greaterOfTwo(int a, int b) {

    int result = a > b ? a : b;
    return result;

}

int smallerOfTwo(int a, int b) {
    int result = a < b ? a : b;
    return result;
}


int moveKnight(int yNod, int xNod, int y, int x, int doIt) {
    int pointT = gameBoard[yNod][xNod].teamIndicator, distinationT = gameBoard[y][x].teamIndicator;

    if (distinationT == pointT)
        return 1;
    if ((((yNod - y) * (yNod - y)) + ((xNod - x) * (xNod - x))) == 5) {
        if (doIt)
            iLikeToMoveIt(yNod, xNod, y, x);
        return 0;
    }

    return 1;  // One = Invalid
}

int checkTrack45(int, int, int, int);

int checkTrack135(int, int, int, int);

int moveBishop(int yNod, int xNod, int y, int x, int doIt) {
    int pointT = gameBoard[yNod][xNod].teamIndicator, distinationT = gameBoard[y][x].teamIndicator;
    int Success = 0;

    if (y - yNod == x - xNod)        // Track One
        Success = checkTrack45(yNod, xNod, y, x);

    else if (y - yNod == xNod - x)   // Track Two
        Success = checkTrack135(yNod, xNod, y, x);

    if (distinationT == pointT)
        Success = 0;

    if (Success == 1) {
        if (doIt)
            iLikeToMoveIt(yNod, xNod, y, x);
        return 0;
    } else if (Success == -1)
        return -1;      // Something Blocking The Way !

    return 1; // One = Invalid
}

int checkTrack45(int yNod, int xNod, int y, int x) {
    int xmin = smallerOfTwo(x, xNod);
    int ymin = smallerOfTwo(y, yNod);
    int ymax = greaterOfTwo(y, yNod);    // Any Of Them Will Do IT .

    for (int i = 1; i < (ymax - ymin); i++)
        if (gameBoard[ymin + i][xmin + i].teamIndicator)
            return -1;

    return 1;
}

int checkTrack135(int yNod, int xNod, int y, int x) {
    int xmin = smallerOfTwo(x, xNod);
    int ymin = smallerOfTwo(y, yNod);
    int xmax = greaterOfTwo(x, xNod);    // Any Of Them Will Do IT .

    for (int i = 1; i < (xmax - xmin); i++)
        if (gameBoard[ymin + i][xmax - i].teamIndicator)
            return -1;

    return 1;
}

int moveQueen(int yNod, int xNod, int y, int x, int doIt) {

    int r = moveRook(yNod, xNod, y, x, 0);
    int b = moveBishop(yNod, xNod, y, x, 0);

    int Failed = r && b;

    if (Failed == 0) {
        if (doIt)
            iLikeToMoveIt(yNod, xNod, y, x);
        return 0;
    }

    if ((r == -1) || (b == -1))
        return -1;

    return 1; // One = Invalid
}

int moveKing(int yNod, int xNod, int y, int x, int doIt) {
    int pointT = gameBoard[yNod][xNod].teamIndicator, distinationT = gameBoard[y][x].teamIndicator;

    if (distinationT == pointT)
        return 1;

    if ((((yNod - y) * (yNod - y)) + ((xNod - x) * (xNod - x))) == 1) {            //Track One
        if (doIt)
            iLikeToMoveIt(yNod, xNod, y, x);
        return 0;
    } else if ((((yNod - y) * (yNod - y)) + ((xNod - x) * (xNod - x))) == 2) {       //Track Two
        if (doIt)
            iLikeToMoveIt(yNod, xNod, y, x);
        return 0;
    }

    return 1; // Non-Zero = Invalid
}

void iLikeToMoveIt(int yNod, int xNod, int y, int x) {

    if (turnS == 0)
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++) {
                gameHistory[0].units[i][j] = gameBoard[i][j].unit;
                gameHistory[0].graveNumbers[i][j] = gameBoard[i][j].graveNumber;
            }

    gameBoard[y][x].unit = gameBoard[yNod][xNod].unit;
    gameBoard[y][x].graveNumber = gameBoard[yNod][xNod].graveNumber;
    gameBoard[y][x].teamIndicator = gameBoard[yNod][xNod].teamIndicator;

    gameBoard[yNod][xNod].unit = '\0';
    gameBoard[yNod][xNod].graveNumber = 0;
    gameBoard[yNod][xNod].teamIndicator = 0;

    turnS++;
    undoS++;

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++) {
            gameHistory[turnS].units[i][j] = gameBoard[i][j].unit;
            gameHistory[turnS].graveNumbers[i][j] = gameBoard[i][j].graveNumber;
        }

    turn = -turn;
    redoS = 0;
}

void findKingPlace(int *, int *, int);

int canTheyReach(int, int, int);

int checkCheck(int nowTurn) {
    int y, x;

    findKingPlace(&y, &x, nowTurn);

    int checks = canTheyReach(y, x, -nowTurn);

    if (checks)
        return 10;                      // Returns 0  : Your Are Safe To Go .. Let The Other Play .
    // Returns 100 : You Checked Your Enemy .
    return 0;
}

void findKingPlace(int *y, int *x, int t) {

    int i, j, found = 0;

    for (i = 0; i < 8; i++) {
        for (j = 0; j < 8; j++) {
            if ((gameBoard[i][j].graveNumber == 5) && (gameBoard[i][j].teamIndicator == t)) {
                found = 1;
                break;
            }
        }
        if (found)
            break;
    }

    if (found) {
        *y = i;
        *x = j;
    }
}

int checkMove(int *, int, int);

int canTheyReach(int y, int x, int t) {

    int sum = 0;
    int move[5] = {0, 0, x, y, 0};

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++) {
            move[0] = i;
            move[1] = j;
            sum += checkMove(move, t, 0);
        }

    return sum;
}

int checkMove(int move[], int turn, int doIT) {
    int xNod = move[0], yNod = move[1], x = move[2], y = move[3], pr = move[4];
    int invalid = 0;

    if (gameBoard[yNod][xNod].teamIndicator == 0)
        return 0;           // Square Is Empty .
    else if (gameBoard[yNod][xNod].teamIndicator != turn)
        return 0;           // Not Your Unit !!

    if ((y == yNod) && (x == xNod))
        return 0;           // Moving To The Same Point !!

    switch (checkUnit(yNod, xNod)) {
        case 1 :
            invalid = movePawn(yNod, xNod, y, x, pr, doIT);
            break;
        case 2 :
            invalid = moveRook(yNod, xNod, y, x, doIT);
            break;
        case 3 :
            invalid = moveKnight(yNod, xNod, y, x, doIT);
            break;
        case 4 :
            invalid = moveBishop(yNod, xNod, y, x, doIT);
            break;
        case 5 :
            invalid = moveQueen(yNod, xNod, y, x, doIT);
            break;
        case 6 :
            invalid = moveKing(yNod, xNod, y, x, doIT);
            break;

        default:
            return 0; // Square Is Empty .
    }

    if (invalid)
        return 0;          // Invalid Move !

    return 1;
}

int checkIfAnyCanMove(int nowTurn) {
    int sum = 0, done = 0/*,found = 0*/, i, j, ii, jj, k;
    int move[5] = {0, 0, 0, 0, 0};

    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++) {
            move[0] = i;
            move[1] = j;
        }

    for (k = 1; k <= 16; k++) {            // grave number for the units .
        for (i = 0; i < 8; i++) {          // y coordinates for alive units i'm looking for .
            for (j = 0; j < 8; j++) {      // x coordinates for alive units i'm looking for .
                if ((gameBoard[i][j].graveNumber == k) &&
                    (gameBoard[i][j].teamIndicator == nowTurn)) {  // alive unit in players turn
                    move[0] = j;    //xNod
                    move[1] = i;    //yNod
                    for (ii = 0; ii < 8; ii++)        // y coordinates for test position .
                        for (jj = 0; jj < 8; jj++) {   // x coordinates for test position .
                            move[2] = jj;
                            move[3] = ii;
                            done = checkMove(move, nowTurn, 1);
                            if (done) {
                                if (checkCheck(nowTurn) == 0)
                                    sum++;
                                undo();
                                redoS = 0;
                                /*
                                if(sum){
                                    printf("\n%c :(%d) can move from %c%d to %c%d\n",gameBoard[i][j].unit,gameBoard[i][j].graveNumber,(move[0]+65),(move[1]+1),(move[2]+65),(move[3]+1));
                                    sum--;
                                    found ++;
                                }*/             //Used for Debug Only . (Won't Delete It ^_^)
                            }
                        }   //end jj loop
                }   //end if for alive units
            }   //end j loop
        }   //end i loop
    }   //end k loop

    if (sum) {
        //printf("\n sum = %d",found);
        /*Sleep(5000);*/                    // Sleep Function : https://youtu.be/MFVuFFQ7Ff8
        return 0;
    }

    return 2000;
}
